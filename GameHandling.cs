﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;


public class GameHandling : MonoBehaviour
{
    [Header("Zur Szene")]
    public string levelName;

    [Header("Main Menu")]
    public GameObject MenuCanvas;
    private bool Paused = false;
    private bool levelWon = false;

    // H
    public GameObject gesamtH;
    private Vector3 solutionPositionH1 = new Vector3(0.375f, -1.6875f, 1.0905f);
    private Vector3 solutionRotationH1 = new Vector3(0.0f, 179.9991f, 0.0f); 
    public GameObject H1;
    private Vector3 solutionPositionH2 = new Vector3(-0.625f, -1.6875f, 1.0905f);
    private Vector3 solutionRotationH2 = new Vector3(0.0f, 179.9991f, 0.0f);
    public GameObject H2;
    private Vector3 solutionPositionH3 = new Vector3(0.399395f, -1.676587f, 2.718266f);
    private Vector3 solutionRotationH3 = new Vector3(0.0f, -0.001215774f, 0.0f); 
    public GameObject H3;
    private Vector3 solutionPositionH4 = new Vector3(-0.600605f, -1.676587f, 2.718266f);
    private Vector3 solutionRotationH4 = new Vector3(0.0f, -0.001215774f, 0.0f);
    public GameObject H4;
    private Vector3 solutionPositionH5 = new Vector3(-0.538105f, -1.676587f, 1.843266f);
    private Vector3 solutionRotationH5 = new Vector3(0.0f, 179.9997f, 0.0f);
    public GameObject H5;
    private Vector3 solutionPositionH6 = new Vector3(0.336895f, -1.676587f, 1.968266f);
    private Vector3 solutionRotationH6 = new Vector3(0.0f, 0.0001f, 0.0f);
    public GameObject H6;

    // rasterCells
    private Vector2Int solRH1;
    private Vector2Int solRH2;
    private Vector2Int solRH3;
    private Vector2Int solRH4;
    private Vector2Int solRH5;
    private Vector2Int solRH6;
    Vector2Int rasterH1;
    Vector2Int rasterH2;
    Vector2Int rasterH3;
    Vector2Int rasterH4;
    Vector2Int rasterH5;
    Vector2Int rasterH6;

    // solutions
    private float solutionDistanceX_h1_h6;
    private float solutionDistanceZ_h1_h6;
    private float solutionDistanceX_h1_h5;
    private float solutionDistanceZ_h1_h5;
    private float solutionDistanceX_h1_h4;
    private float solutionDistanceZ_h1_h4;
    private float solutionDistanceX_h1_h3;
    private float solutionDistanceZ_h1_h3;
    private float solutionDistanceX_h1_h2;
    private float solutionDistanceZ_h1_h2;

    // SPIELFELD
    private float w = 10.0f;
    private float h = 5.0f;
    private Vector2 topLeft = new Vector2(0.0f, 0.0f);
    private int level = 0;
    private float rasterSize = 0.5f;

    // Start is called before the first frame update
    void Start()
    {
        H1 = GameObject.Find("H1");
        H2 = GameObject.Find("H2");
        H3 = GameObject.Find("H3");
        H4 = GameObject.Find("H4");
        H5 = GameObject.Find("H5");
        H6 = GameObject.Find("H6");

        // top left

        //solution
        solRH1 = getRasterCell(solutionPositionH1);
        solRH2 = getRasterCell(solutionPositionH2);
        solRH3 = getRasterCell(solutionPositionH3);
        solRH4 = getRasterCell(solutionPositionH4);
        solRH5 = getRasterCell(solutionPositionH5);
        solRH6 = getRasterCell(solutionPositionH6);
        Debug.Log("SOLUTION: H1 in (" + solRH1.x + ", " + solRH1.y + "), " + 
            "H2 in (" + solRH2.x + ", " + solRH2.y + ")," + 
            "H3 in (" + solRH3.x + ", " + solRH3.y + ")," + 
            "H4 in (" + solRH4.x + ", " + solRH4.y + ")," +
            "H5 in (" + solRH5.x + ", " + solRH5.y + ")," +
            "H6 in (" + solRH6.x + ", " + solRH6.y + ")");

        solutionDistanceX_h1_h6 = solRH1.x - solRH6.x;
        solutionDistanceZ_h1_h6 = solRH1.y - solRH6.y;
        Debug.Log("H1-6.x : " + solutionDistanceX_h1_h6 + ", H1-6.z : " + solutionDistanceZ_h1_h6);
        solutionDistanceX_h1_h5 = solRH1.x - solRH5.x;
        solutionDistanceZ_h1_h5 = solRH1.y - solRH5.y;
        Debug.Log("H1-5.x : " + solutionDistanceX_h1_h5 + ", H1-5.z : " + solutionDistanceZ_h1_h5);
        solutionDistanceX_h1_h4 = solRH1.x - solRH4.x;
        solutionDistanceZ_h1_h4 = solRH1.y - solRH4.y;
        Debug.Log("H1-4.x : " + solutionDistanceX_h1_h4 + ", H1-4.z : " + solutionDistanceZ_h1_h4);
        solutionDistanceX_h1_h3 = solRH1.x - solRH3.x;
        solutionDistanceZ_h1_h3 = solRH1.y - solRH3.y;
        Debug.Log("H1-3.x : " + solutionDistanceX_h1_h3 + ", H1-3.z : " + solutionDistanceZ_h1_h3);
        solutionDistanceX_h1_h2 = solRH1.x - solRH2.x;
        solutionDistanceZ_h1_h2 = solRH1.y - solRH2.y;
        Debug.Log("H1-2.x : " + solutionDistanceX_h1_h2 + ", H1-2.z : " + solutionDistanceZ_h1_h2);
    }

    // Update is called once per frame
    void Update()
    {
        // ---------------------------------------------------------------- Main Menu
        if (Input.GetKey("escape"))
        {
            if (Paused == true)
            {
                //Time.timeScale = 1.0f;
                MenuCanvas.SetActive(false);
                Paused = false;
            }
            else
            {
                //Time.timeScale = 0.0f;
                MenuCanvas.SetActive(true);
                Paused = true;
            }
        }
        if (Paused == false)
        {
            // ---------------------------------------------------------------- Check if pieces match
            bool[] matchingPieces = getMatchingPieces(H1, H2, H3, H4, H5, H6, 6);
        }
        else
        {
            if (levelWon)
            {
                Debug.Log("WON!");
            }
            else 
            {
                Debug.Log("Something weird!");
            }
        }
        
    }

    // -------------------------------------------------------------------- Check if pieces match
    public Vector2Int getRasterCell(Vector3 pos)
    {
        // y is the plane of the game
        Vector2 absolutePosition = new Vector2(pos.x, pos.z);
        Vector2 relativePosition = absolutePosition - topLeft;
        int i = (int)(Mathf.RoundToInt(relativePosition.x / rasterSize));
        int j = (int)(Mathf.RoundToInt(relativePosition.y / rasterSize));
        Vector2Int rasterCell = new Vector2Int(i,j);
        return rasterCell;
    }

    public bool[] getMatchingPieces(GameObject h1, GameObject h2, GameObject h3, GameObject h4, GameObject h5, GameObject h6, int pieces)
    {
        bool[] matching = new bool[pieces];

        // positions in the raster
        rasterH1 = getRasterCell(h1.transform.position);
        rasterH2 = getRasterCell(h2.transform.position);
        rasterH3 = getRasterCell(h3.transform.position);
        rasterH4 = getRasterCell(h4.transform.position);
        rasterH5 = getRasterCell(h5.transform.position);
        rasterH6 = getRasterCell(h6.transform.position);
        Debug.Log("H1 is in (" + rasterH1.x + ", " + rasterH1.y + "), " + 
            "H2 is in (" + rasterH2.x + ", " + rasterH2.y + ")," + 
            "H3 is in (" + rasterH3.x + ", " + rasterH3.y + ")," + 
            "H4 is in (" + rasterH4.x + ", " + rasterH4.y + ")," +
            "H5 is in (" + rasterH5.x + ", " + rasterH5.y + ")," +
            "H6 is in (" + rasterH6.x + ", " + rasterH6.y + ")");

        int roundedRot = (int)(Mathf.RoundToInt(h1.transform.eulerAngles.y));
        float solutionDistanceX = 0.0f;
        float solutionDistanceZ = 0.0f;
        Debug.Log("rotation" + roundedRot);
        switch(roundedRot)
        {
            case 0:
                if (Math.Abs((rasterH1.y - rasterH6.y) - solutionDistanceZ_h1_h6) < rasterSize 
                    && Math.Abs((rasterH6.x - rasterH1.x) - solutionDistanceX_h1_h6) < rasterSize)
                {
                    // h1 and h6 are good
                    //Debug.Log("h1 and h6 are good!");
                    if (Math.Abs((rasterH1.y - rasterH5.y) - solutionDistanceZ_h1_h5) < rasterSize 
                        && Math.Abs((rasterH5.x - rasterH1.x) - solutionDistanceX_h1_h5) < rasterSize)
                    {
                        // h1 and h5 are good
                        //Debug.Log("h1 and h5 are good!");
                        // check H1 and H4
                        if (Math.Abs((rasterH1.y - rasterH4.y) - solutionDistanceZ_h1_h4) < rasterSize  
                            && Math.Abs((rasterH5.x - rasterH1.x) - solutionDistanceX_h1_h4) < rasterSize)
                        {
                            // h1 and h4 are good
                            //Debug.Log("h1 and h4 are good!");
                            // check H1 and H3
                            if (Math.Abs((rasterH1.y - rasterH3.y) - solutionDistanceZ_h1_h3) < rasterSize 
                                && Math.Abs((rasterH3.x - rasterH1.x) - solutionDistanceX_h1_h3) < rasterSize)
                            {
                                // h1 and h3 are good
                                //Debug.Log("h1 and h3 are good!");
                                // check H1 and H2
                                if (Math.Abs((rasterH1.y - rasterH2.y) - solutionDistanceZ_h1_h2) < rasterSize 
                                    && Math.Abs((rasterH2.x - rasterH1.x) - solutionDistanceX_h1_h2) < rasterSize)
                                {
                                    levelWon = true;
                                    Debug.Log("GEWONNEN!");
                                    level += 1;
                                }
                            }
                        }
                    }
                }
                break;
            case 90:
                Debug.Log("TODO!");
                break;
            case 180:
                //Debug.Log("X: " + solutionDistanceX_h1_h6 + " : " + rasterH1.x + " - " + rasterH6.x + " / " + rasterSize);
                //Debug.Log("Z: " + solutionDistanceZ_h1_h6 + " : " + rasterH1.y + " - " + rasterH6.y + " / " + rasterSize);
                //Debug.Log("resultz = " + ((rasterH1.y - rasterH6.y) - solutionDistanceZ_h1_h6) + 
                //    ", resultx = " + ((rasterH1.x - rasterH6.x) - solutionDistanceX_h1_h6));
                if (((rasterH1.y - rasterH6.y) - solutionDistanceZ_h1_h6) < rasterSize 
                    && ((rasterH1.x - rasterH6.x) - solutionDistanceX_h1_h6) < rasterSize)
                {
                    // h1 and h6 are good
                    //Debug.Log("h1 and h6 are good! case 180");
                    // check H1 and H5
                    if (((rasterH1.y - rasterH5.y) - solutionDistanceZ_h1_h5) < rasterSize 
                        && ((rasterH1.x - rasterH5.x) - solutionDistanceX_h1_h5) < rasterSize)
                    {
                        // h1 and h5 are good
                        //Debug.Log("h1 and h5 are good!");
                        // check H1 and H4
                        if (((rasterH1.y - rasterH4.y) - solutionDistanceZ_h1_h4) < rasterSize 
                            && ((rasterH1.x - rasterH4.x) - solutionDistanceX_h1_h4) < rasterSize)
                        {
                            // h1 and h4 are good
                            //Debug.Log("h1 and h4 are good!");
                            // check H1 and H3
                            if (((rasterH1.y - rasterH3.y) - solutionDistanceZ_h1_h3) < rasterSize 
                                && ((rasterH1.x - rasterH3.x) - solutionDistanceX_h1_h3) < rasterSize)
                            {
                                // h1 and h3 are good
                                //Debug.Log("h1 and h3 are good!");
                                // check H1 and H2
                                if (Math.Abs((rasterH2.y - rasterH1.y) - solutionDistanceZ_h1_h2) < rasterSize 
                                    && Math.Abs((rasterH1.x - rasterH2.x) - solutionDistanceX_h1_h2) < rasterSize)
                                {
                                    levelWon = true;
                                    Debug.Log("GEWONNEN!");
                                    level += 1;
                                }
                            }
                        }
                    }
                }
                break;
            case 270:
                Debug.Log("TODO!");
                break;

        }

        return matching;
    }

    // ---------------------------------------------------------------- Resume Menu function
    public void Resume()
    {
        Time.timeScale = 1.0f;
        MenuCanvas.SetActive(false);
    }

    // ---------------------------------------------------------------- To Main Menu function
    public void toMainMenu()
    {
        SceneManager.LoadScene(levelName);
    }


}
