﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using System.Linq;
using System.Text;
using System.IO;


public class GameController : MonoBehaviour
{
    [Header("Zur Szene")]
    public string levelName;

    [Header("Main Menu")]
    public GameObject MenuCanvas;
    private bool Paused = false;
    private bool levelWon = false;
    public float yOffset = 0;

    // H
    public GameObject gesamtH;
    private Vector3 solutionPositionH1 = new Vector3(0.375f, -1.6875f, 1.0905f);
    private Vector3 solutionRotationH1 = new Vector3(0.0f, 179.9991f, 0.0f);
    public GameObject H1;
    private Vector3 solutionPositionH2 = new Vector3(-0.625f, -1.6875f, 1.0905f);
    private Vector3 solutionRotationH2 = new Vector3(0.0f, 179.9991f, 0.0f);
    public GameObject H2;
    private Vector3 solutionPositionH3 = new Vector3(0.399395f, -1.676587f, 2.718266f);
    private Vector3 solutionRotationH3 = new Vector3(0.0f, -0.001215774f, 0.0f);
    public GameObject H3;
    private Vector3 solutionPositionH4 = new Vector3(-0.600605f, -1.676587f, 2.718266f);
    private Vector3 solutionRotationH4 = new Vector3(0.0f, -0.001215774f, 0.0f);
    public GameObject H4;
    private Vector3 solutionPositionH5 = new Vector3(-0.538105f, -1.676587f, 1.843266f);
    private Vector3 solutionRotationH5 = new Vector3(0.0f, 179.9997f, 0.0f);
    public GameObject H5;
    private Vector3 solutionPositionH6 = new Vector3(0.336895f, -1.676587f, 1.968266f);
    private Vector3 solutionRotationH6 = new Vector3(0.0f, 0.0001f, 0.0f);
    public GameObject H6;

    // rasterCells
    // SOL 1
    // private Vector2Int solRH1 = new Vector2Int(-5, 4); // new Vector2Int(-8, 5);
    // private Vector2Int solRH2 = new Vector2Int(-11, -4); //new Vector2Int(-14, -3);
    // private Vector2Int solRH3 = new Vector2Int(-9, 4); // new Vector2Int(-10, -3);
    // private Vector2Int solRH4 = new Vector2Int(-7, -4); //new Vector2Int(-12, 5);
    // private Vector2Int solRH5 = new Vector2Int(-5, 3); // new Vector2Int(-14, -2);
    // private Vector2Int solRH6 = new Vector2Int(-11, -3); //new Vector2Int(-8, 4);
    // SOL 2
    private Vector2Int solRH1 = new Vector2Int(-8, 5);
    private Vector2Int solRH2 = new Vector2Int(-14, -3);
    private Vector2Int solRH3 = new Vector2Int(-10, -3);
    private Vector2Int solRH4 = new Vector2Int(-12, 5);
    private Vector2Int solRH5 = new Vector2Int(-14, -2);
    private Vector2Int solRH6 = new Vector2Int(-8, 4);
    Vector2Int rasterH1;
    Vector2Int rasterH2;
    Vector2Int rasterH3;
    Vector2Int rasterH4;
    Vector2Int rasterH5;
    Vector2Int rasterH6;

    // start
    Vector2Int rH1_start = new Vector2Int(7, 2);
    Vector2Int rH2_start = new Vector2Int(5, 2);
    Vector2Int rH3_start = new Vector2Int(7, 5);
    Vector2Int rH4_start = new Vector2Int(5, 5);
    Vector2Int rH5_start = new Vector2Int(5, 3);
    Vector2Int rH6_start = new Vector2Int(7, 3);

    // solutions
    private float solutionDistanceX_h1_h6;
    private float solutionDistanceZ_h1_h6;
    private float solutionDistanceX_h1_h5;
    private float solutionDistanceZ_h1_h5;
    private float solutionDistanceX_h1_h4;
    private float solutionDistanceZ_h1_h4;
    private float solutionDistanceX_h1_h3;
    private float solutionDistanceZ_h1_h3;
    private float solutionDistanceX_h1_h2;
    private float solutionDistanceZ_h1_h2;

    // SPIELFELD
    private float w = 10.0f;
    private float h = 5.0f;
    private Vector2 topLeft = new Vector2(0.0f, 0.0f);
    private int level = 0;
    private float rasterSize = 0.25f;

    Vector2 topLeftMap;
    Vector2 bottomRightMap;
    int h_cells;
    int w_cells;
    RaycastHit hit;
    int[,] map;
    bool[,] cur_sit;
    int[,] cur_map;
    int[,] cur_diff;
    Vector3 originTmp; // = new Vector3(-2.8f, 2.25f, 0.2f);
    Vector3 topLeftWC;
    Vector3 shift;
    float radius;
    Collider[] hitColliders;
    bool patternFound;

    // PATTERN
    int[,] patternH_int;

    bool[,] patternH = new bool[,]
    {
        {true, true, true, false, true, true, true},
        {true, true, true, false, true, true, true},
        {true, true, true, false, true, true, true},
        {true, true, true, true, true, true, true},
        {true, true, true, true, true, true, true},
        {true, true, true, true, true, true, true},
        {true, true, true, true, true, true, true},
        {true, true, true, false, true, true, true},
        {true, true, true, false, true, true, true}
    };

    // Start is called before the first frame update
    void Start()
    {
        H1 = GameObject.Find("H1new");
        H2 = GameObject.Find("H2new");
        H3 = GameObject.Find("H3new");
        H4 = GameObject.Find("H4new");
        H5 = GameObject.Find("H5new");
        H6 = GameObject.Find("H6new");

        // top left

        //solution
        //solRH1 = getRasterCell(solutionPositionH1);
        //solRH2 = getRasterCell(solutionPositionH2);
        //solRH3 = getRasterCell(solutionPositionH3);
        //solRH4 = getRasterCell(solutionPositionH4);
        //solRH5 = getRasterCell(solutionPositionH5);
        //solRH6 = getRasterCell(solutionPositionH6);
        // Debug.Log("SOLUTION: H1 in (" + solRH1.x + ", " + solRH1.y + "), " +
        //     "H2 in (" + solRH2.x + ", " + solRH2.y + ")," +
        //     "H3 in (" + solRH3.x + ", " + solRH3.y + ")," +
        //     "H4 in (" + solRH4.x + ", " + solRH4.y + ")," +
        //     "H5 in (" + solRH5.x + ", " + solRH5.y + ")," +
        //     "H6 in (" + solRH6.x + ", " + solRH6.y + ")");

        // H2.transform.position = getPosition(solRH2);
        // H3.transform.position = getPosition(solRH3);
        // H4.transform.position = getPosition(solRH4);
        // H5.transform.position = getPosition(solRH5);
        // H6.transform.position = getPosition(solRH6);

        // Debug.Log("H2 position" + H2.transform.position);
        // Debug.Log("H3 position" + H3.transform.position);

        solutionDistanceX_h1_h6 = solRH1.x - solRH6.x;
        solutionDistanceZ_h1_h6 = solRH1.y - solRH6.y;
        Debug.Log("H1-6.x : " + solutionDistanceX_h1_h6 + ", H1-6.z : " + solutionDistanceZ_h1_h6);
        solutionDistanceX_h1_h5 = solRH1.x - solRH5.x;
        solutionDistanceZ_h1_h5 = solRH1.y - solRH5.y;
        Debug.Log("H1-5.x : " + solutionDistanceX_h1_h5 + ", H1-5.z : " + solutionDistanceZ_h1_h5);
        solutionDistanceX_h1_h4 = solRH1.x - solRH4.x;
        solutionDistanceZ_h1_h4 = solRH1.y - solRH4.y;
        Debug.Log("H1-4.x : " + solutionDistanceX_h1_h4 + ", H1-4.z : " + solutionDistanceZ_h1_h4);
        solutionDistanceX_h1_h3 = solRH1.x - solRH3.x;
        solutionDistanceZ_h1_h3 = solRH1.y - solRH3.y;
        Debug.Log("H1-3.x : " + solutionDistanceX_h1_h3 + ", H1-3.z : " + solutionDistanceZ_h1_h3);
        solutionDistanceX_h1_h2 = solRH1.x - solRH2.x;
        solutionDistanceZ_h1_h2 = solRH1.y - solRH2.y;
        Debug.Log("H1-2.x : " + solutionDistanceX_h1_h2 + ", H1-2.z : " + solutionDistanceZ_h1_h2);

        topLeftMap = new Vector2(-5.46f, 3.08f);
        bottomRightMap = new Vector2(5.56f, -3.21f);
        h_cells = (int)Math.Round((bottomRightMap.x - topLeftMap.x) / rasterSize);
        w_cells = (int)Math.Round((topLeftMap.y - bottomRightMap.y) / rasterSize);
        Debug.Log("H: " + h_cells + ", W: " + w_cells);
        map = new int[h_cells, w_cells];
        cur_sit = new bool[h_cells, w_cells];
        cur_map = new int[h_cells, w_cells];
        cur_diff = new int[h_cells, w_cells];
        topLeftWC = new Vector3(topLeftMap.x, 0.0f, topLeftMap.y);
        radius = .5f * rasterSize;

        patternH_int = new int[,] {
            {1, 1, 1, 0, 1, 1, 1},
            {1, 1, 1, 0, 1, 1, 2},
            {1, 1, 2, 0, 1, 2, 1},
            {1, 2, 1, 1, 3, 1, 1},
            {2, 1, 1, 2, 1, 1, 2},
            {1, 1, 3, 1, 1, 2, 1},
            {1, 2, 1, 0, 2, 1, 1},
            {2, 1, 1, 0, 1, 1, 1},
            {1, 1, 1, 0, 1, 1, 1}
        };
    }

    // Update is called once per frame
    void Update()
    {
        // ---------------------------------------------------------------- Main Menu
        if (Input.GetKey("escape"))
        {
            if (Paused == true)
            {
                //Time.timeScale = 1.0f;
                MenuCanvas.SetActive(false);
                Paused = false;
            }
            else
            {
                //Time.timeScale = 0.0f;
                MenuCanvas.SetActive(true);
                Paused = true;
            }
        }
        if (Input.GetKeyDown(KeyCode.U))
        {
            saveSolution();
        }
        if (Input.GetKeyDown(KeyCode.H))
        {
            bool correct = checkSolution(patternH, 7, 9);
            if (correct)
            {
                Debug.Log("WWWWWOOON!!!!");
            }
        }

        if (Paused == false)
        {
            // ---------------------------------------------------------------- Check if pieces match
            //bool[] matchingPieces = getMatchingPieces(H1, H2, H3, H4, H5, H6, 6);
        }
        else
        {
            if (levelWon)
            {
                //Debug.Log("WON!");
            }
            else
            {
                Debug.Log("Something weird!");
            }
        }

    }

    // -------------------------------------------------------------------- Save .txt with solution
    public void saveSolution()
    {
        //t1.transform.position = topLeftWC + new Vector3(0.0f, -1.5f, 0.0f);
        //t2.transform.position = topLeftWC + new Vector3(h_cells*rasterSize, -1.5f, -w_cells*rasterSize);
        Paused = true;
        //hitColliders = Physics.OverlapSphere(originTmp, radius);          
        // int ij = 0;  
        // while (ij < hitColliders.Length)
        // {
        //     Debug.Log("NO LOOP: Overlap with " + hitColliders[ij]); //.SendMessage("AddDamage");
        //     ij++;
        // }
        int ij = 0;
        for (int i = 0; i < h_cells; i++)
        {
            for (int j = 0; j < w_cells; j++)
            {
                shift = new Vector3(i * rasterSize, yOffset, -j * rasterSize);
                originTmp = topLeftWC + shift;
                //originTmp = new Vector3(0.0f, 0.75f, 1.25f);
                //Debug.Log("originTmp" + originTmp);
                ij = 0;

                hitColliders = Physics.OverlapSphere(originTmp, radius);
                while (ij < hitColliders.Length)
                {
                    //Debug.Log("At raster cell (" + i + ", " + j + "), with 3D position " + originTmp + ", overlap with " + hitColliders[ij] + " with position " + hitColliders[ij].transform.position); //.SendMessage("AddDamage");
                    ij++;
                }
                map[i, j] = ij;
            }
        }
        // StreamWriter writer = new StreamWriter("/Users/Palma/Documents/Projects/GameJam2020/H_solution2.txt", false);
        // string line;
        // for (int j=0; j<w_cells; j++)
        // {
        //     line = "";
        //     for (int i=0; i<h_cells; i++)
        //     {
        //         line = line + map[i,j] + ", ";
        //     }
        //     writer.WriteLine(line);
        // }
        // writer.Close();
    }

    // -------------------------------------------------------------------- Check if pieces match
    public bool checkSolution(bool[,] pattern, int l_i, int l_j)
    {
        //Debug.Log("Checking!");
        //Paused = true;
        int i_half_pat_size = (int)(Mathf.Floor(l_i / 2));
        int j_half_pat_size = (int)(Mathf.Floor(l_j / 2));
        string colliderName;
        int ij = 0;
        int diff = 0;
        for (int i = 0; i < h_cells-1; i++)
        {
            for (int j = 0; j < w_cells-1; j++)
            {
                shift = new Vector3(i * rasterSize, yOffset, -j * rasterSize);
                originTmp = topLeftWC + shift;
                //originTmp = new Vector3(0.0f, 0.75f, 1.25f);
                //Debug.Log("originTmp" + originTmp);
                ij = 0;
                int counter = 0;
                hitColliders = Physics.OverlapSphere(originTmp, radius);
                while (ij < hitColliders.Length)
                {
                    //Debug.Log("At raster cell (" + i + ", " + j + "), with 3D position " + originTmp + ", overlap with " + hitColliders[ij] + " with position " + hitColliders[ij].transform.position); //.SendMessage("AddDamage");
                    if (Movable.getMovable(hitColliders[ij].gameObject))
                    {
                        counter++;
                    }
                    ij++;
                }
                cur_map[i, j] = counter;
                if (ij > 0)
                    cur_sit[i, j] = true;
                else
                    cur_sit[i, j] = false;
            }
        }
        // StreamWriter writer = new StreamWriter("/Users/Palma/Documents/Projects/GameJam2020/H_cur_map.txt", false);
        // string line;
        // for (int j=0; j<w_cells; j++)
        // {
        //     line = "";
        //     for (int i=0; i<h_cells; i++)
        //     {
        //         line = line + cur_map[i,j] + ", ";
        //     }
        //     writer.WriteLine(line);
        // }
        // writer.Close();
        patternFound = false;
        // jetzt checken
        bool found = false;
        for (int i = i_half_pat_size + 1; i < h_cells - l_i - 1; i++)
        {
            for (int j = j_half_pat_size + 1; j < w_cells - l_j - 1; j++)
            {
                if (cur_map[i, j] > 0)
                {
                    //Debug.Log("checking at " + i + ", " + j);
                    diff = checkPatternI(patternH_int, cur_map, i, j, l_i, l_j);
                    if (diff < 1)
                    {
                        patternFound = true;
                    }
                    cur_diff[i, j] = diff;
                }
            }
        }
        // writer = new StreamWriter("/Users/Palma/Documents/Projects/GameJam2020/H_cur_diff.txt", false);
        // for (int j=0; j<w_cells; j++)
        // {
        //     line = "";
        //     for (int i=0; i<h_cells; i++)
        //     {
        //         line = line + cur_diff[i,j] + ", ";
        //     }
        //     writer.WriteLine(line);
        // }
        // writer.Close();
        Debug.Log("Found? " + patternFound);
        return patternFound;
    }

    bool checkPattern(bool[,] pattern, bool[,] map, int x, int y, int l_x, int l_y)
    {
        int diff = 0;
        for (int i = 0; i < l_x - 1; i++)
        {
            for (int j = 0; j < l_y - 1; j++)
            {
                if (pattern[i, j] != map[x + i, y + j])
                {
                    diff += 1;
                }
            }
        }

        if (diff < 1)
            return true;
        //Debug.Log("Diff: " + diff);
        return false;
    }

    int checkPatternI(int[,] pattern, int[,] map, int x, int y, int l_x, int l_y)
    {
        int diff = 0;
        //Debug.Log("At " + x + ", " + y + ", l_x " + l_x + ", l_y " + l_y);
        //Debug.Log(pattern);
        for (int i = 0; i < (l_x - 1); i++)
        {
            for (int j = 0; j < l_y - 1; j++)
            {
                //Debug.Log("At " + i + ", " + j + " l " + l_x);
                //Debug.Log("At " + i + ", " + j + "pattern: " + patternH_int[i,j]);
                if (pattern[j, i] > 0 && map[x + i, y + j] == 0 || pattern[j, i] == 0 && map[x + i, y + j] > 0)
                {
                    diff += 1; //(Math.Abs(pattern[j,i] - map[x+i,y+j]));
                }
            }
        }
        //Debug.Log("At " + x + ", " + y + ", diff: " + diff);
        return diff;
    }

    // -------------------------------------------------------------------- Get Raster Cell from 3D Position
    public Vector2Int getRasterCell(Vector3 pos)
    {
        // y is the plane of the game
        Vector2 absolutePosition = new Vector2(pos.x, pos.z);
        Vector2 relativePosition = absolutePosition - topLeft;
        int i = (int)(Mathf.RoundToInt(relativePosition.x / rasterSize));
        int j = (int)(Mathf.RoundToInt(relativePosition.y / rasterSize));
        Vector2Int rasterCell = new Vector2Int(i, j);
        return rasterCell;
    }

    // -------------------------------------------------------------------- Get 3D Position from Raster Cell
    public Vector3 getPosition(Vector2Int rasterCell)
    {
        float x_position = rasterCell.x * rasterSize + topLeft.x;
        float z_position = rasterCell.y * rasterSize + topLeft.y;
        Vector3 xyz_position = new Vector3(x_position, 0.0f, z_position);
        return xyz_position;
    }

    public bool[] getMatchingPieces(GameObject h1, GameObject h2, GameObject h3, GameObject h4, GameObject h5, GameObject h6, int pieces)
    {
        bool[] matching = new bool[pieces];

        // positions in the raster
        rasterH1 = getRasterCell(h1.transform.position);
        rasterH2 = getRasterCell(h2.transform.position);
        rasterH3 = getRasterCell(h3.transform.position);
        rasterH4 = getRasterCell(h4.transform.position);
        rasterH5 = getRasterCell(h5.transform.position);
        rasterH6 = getRasterCell(h6.transform.position);
        // Debug.Log("H1 is in (" + rasterH1.x + ", " + rasterH1.y + "), " + 
        //     "H2 is in (" + rasterH2.x + ", " + rasterH2.y + ")," + 
        //     "H3 is in (" + rasterH3.x + ", " + rasterH3.y + ")," + 
        //     "H4 is in (" + rasterH4.x + ", " + rasterH4.y + ")," +
        //     "H5 is in (" + rasterH5.x + ", " + rasterH5.y + ")," +
        //     "H6 is in (" + rasterH6.x + ", " + rasterH6.y + ")");

        int roundedRot = (int)(Mathf.RoundToInt(h1.transform.eulerAngles.y));
        float solutionDistanceX = 0.0f;
        float solutionDistanceZ = 0.0f;
        //Debug.Log("rotation" + roundedRot);
        switch (roundedRot)
        {
            case 0:
                if (Math.Abs((rasterH1.y - rasterH6.y) - solutionDistanceZ_h1_h6) < rasterSize
                    && Math.Abs((rasterH6.x - rasterH1.x) - solutionDistanceX_h1_h6) < rasterSize)
                {
                    // h1 and h6 are good
                    //Debug.Log("h1 and h6 are good!");
                    if (Math.Abs((rasterH1.y - rasterH5.y) - solutionDistanceZ_h1_h5) < rasterSize
                        && Math.Abs((rasterH5.x - rasterH1.x) - solutionDistanceX_h1_h5) < rasterSize)
                    {
                        // h1 and h5 are good
                        //Debug.Log("h1 and h5 are good!");
                        // check H1 and H4
                        if (Math.Abs((rasterH1.y - rasterH4.y) - solutionDistanceZ_h1_h4) < rasterSize
                            && Math.Abs((rasterH5.x - rasterH1.x) - solutionDistanceX_h1_h4) < rasterSize)
                        {
                            // h1 and h4 are good
                            //Debug.Log("h1 and h4 are good!");
                            // check H1 and H3
                            if (Math.Abs((rasterH1.y - rasterH3.y) - solutionDistanceZ_h1_h3) < rasterSize
                                && Math.Abs((rasterH3.x - rasterH1.x) - solutionDistanceX_h1_h3) < rasterSize)
                            {
                                // h1 and h3 are good
                                //Debug.Log("h1 and h3 are good!");
                                // check H1 and H2
                                if (Math.Abs((rasterH1.y - rasterH2.y) - solutionDistanceZ_h1_h2) < rasterSize
                                    && Math.Abs((rasterH2.x - rasterH1.x) - solutionDistanceX_h1_h2) < rasterSize)
                                {
                                    levelWon = true;
                                    //Debug.Log("GEWONNEN!");
                                    //level += 1;
                                }
                            }
                        }
                    }
                }
                break;
            case 90:
                Debug.Log("TODO!");
                break;
            case 180:
                //Debug.Log("X: " + solutionDistanceX_h1_h6 + " : " + rasterH1.x + " - " + rasterH6.x + " / " + rasterSize);
                //Debug.Log("Z: " + solutionDistanceZ_h1_h6 + " : " + rasterH1.y + " - " + rasterH6.y + " / " + rasterSize);
                //Debug.Log("resultz = " + ((rasterH1.y - rasterH6.y) - solutionDistanceZ_h1_h6) + 
                //    ", resultx = " + ((rasterH1.x - rasterH6.x) - solutionDistanceX_h1_h6));
                if (((rasterH1.y - rasterH6.y) - solutionDistanceZ_h1_h6) < rasterSize
                    && ((rasterH1.x - rasterH6.x) - solutionDistanceX_h1_h6) < rasterSize)
                {
                    // h1 and h6 are good
                    //Debug.Log("h1 and h6 are good! case 180");
                    // check H1 and H5
                    if (((rasterH1.y - rasterH5.y) - solutionDistanceZ_h1_h5) < rasterSize
                        && ((rasterH1.x - rasterH5.x) - solutionDistanceX_h1_h5) < rasterSize)
                    {
                        // h1 and h5 are good
                        //Debug.Log("h1 and h5 are good!");
                        // check H1 and H4
                        if (((rasterH1.y - rasterH4.y) - solutionDistanceZ_h1_h4) < rasterSize
                            && ((rasterH1.x - rasterH4.x) - solutionDistanceX_h1_h4) < rasterSize)
                        {
                            // h1 and h4 are good
                            //Debug.Log("h1 and h4 are good!");
                            // check H1 and H3
                            if (Math.Abs((rasterH1.y - rasterH3.y) - solutionDistanceZ_h1_h3) < rasterSize
                                && Math.Abs((rasterH1.x - rasterH3.x) - solutionDistanceX_h1_h3) < rasterSize)
                            {
                                // h1 and h3 are good
                                //Debug.Log(rasterH3 + ", " + rasterH1);
                                //Debug.Log("h1 and h3 are good!");
                                // check H1 and H2
                                if (((rasterH1.y - rasterH2.y) - solutionDistanceZ_h1_h2) < rasterSize
                                    && ((rasterH1.x - rasterH2.x) - solutionDistanceX_h1_h2) < rasterSize)
                                {
                                    levelWon = true;
                                    //Debug.Log("GEWONNEN!");
                                    //level += 1;
                                }
                            }
                        }
                    }
                }
                break;
            case 270:
                Debug.Log("TODO!");
                break;

        }

        return matching;
    }

    // ---------------------------------------------------------------- Resume Menu function
    public void Resume()
    {
        Time.timeScale = 1.0f;
        MenuCanvas.SetActive(false);
    }

    // ---------------------------------------------------------------- To Main Menu function
    public void toMainMenu()
    {
        SceneManager.LoadScene(levelName);
    }

    public bool isPaused()
    {
        return Paused;
    }
}
