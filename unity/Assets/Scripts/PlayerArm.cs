﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerArm : MonoBehaviour
{
    private Movable movable = null;
    private bool hold = false;
    private Player player = null;
    public AudioClip grab;
    public AudioClip drop;

    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<Player>();
        this.gameObject.AddComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if(movable != null && player.getCurrentMove() == Player.Move.NONE && !FindObjectOfType<GameController>().isPaused())
        {
            if(Input.GetKeyDown(KeyCode.Space))
            {
                hold = !hold;
                if (hold) { 
                    movable.setHighlighted(true);
                    this.gameObject.GetComponent<AudioSource>().PlayOneShot(grab, (float) 0.5);
                }
                else
                {
                    movable.setHighlighted(false);
                    this.gameObject.GetComponent<AudioSource>().PlayOneShot(drop, (float) 2.0);
                }
            }
        }
    }

    public Movable getConnectedMovable()
    {
        return hold ? movable : null;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(!hold)
        {
            movable = Movable.getMovable(other.gameObject);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (!hold)
        {
            if (movable)
                movable.setHighlighted(false);
            movable = null;
            hold = false;
        }
    }
}
