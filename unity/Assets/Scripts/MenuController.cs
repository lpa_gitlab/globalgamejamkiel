﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class MenuController : MonoBehaviour
{
    [Header("Zur Szene")]
    public string levelName;

    public void ExitGame()
    {
        GetComponent<AudioSource>().Play();
        Application.Quit();
    }

    public void NextScene()
    {
        GetComponent<AudioSource>().Play();
        SceneManager.LoadScene(levelName);
    }
}
