﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Player : MonoBehaviour
{
    [Header("Bewegungs-Werte")]
    public float speed = 1.0f;
    public float rotationSpeed = 100.0f;
    public float animationStart = 0;
    public float rasterSize = 0.0635f;
    public bool reversed = false;
    private Vector3 oldPosition;
    private PlayerArm arm;
    private LinkedList<Movable> pushed = new LinkedList<Movable>();
    private Vector3 moveStartPoint;

    Animator anim;
    int moveHash = Animator.StringToHash("HainMove");

    public enum Move
    {
        NONE,
        FRONT,
        BACK,
        ROTATE_LEFT,
        ROTATE_RIGHT
    }

    private Move move = Move.NONE;

    // Start is called before the first frame update
    void Start()
    {
        arm = FindObjectOfType<PlayerArm>();
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        oldPosition = transform.position;
        Movable movable = arm.getConnectedMovable();
        if (move == Move.NONE && !FindObjectOfType<GameController>().isPaused())
        {
            if (Input.GetAxisRaw("Vertical") == 1)
                move = Move.FRONT;
            if (Input.GetAxisRaw("Vertical") == -1)
                move = Move.BACK;
            if (Input.GetAxisRaw("Horizontal") == 1)
                move = Move.ROTATE_RIGHT;
            if (Input.GetAxisRaw("Horizontal") == -1)
                move = Move.ROTATE_LEFT;
            animationStart = 0;
            moveStartPoint = transform.position;
            while (pushed.Count > 0)
                pushed.RemoveFirst();
            reversed = false;
        }
        if (move == Move.FRONT)
        {
            anim.SetBool("HainMove", true);
        }
        else
        {
            anim.SetBool("HainMove", false);
        }
        if (move == Move.FRONT || move == Move.BACK)
        {
            float delta = Time.deltaTime;
            animationStart += Time.deltaTime;
            bool front = move == Move.FRONT;
            if (animationStart * speed > rasterSize)
            {
                animationStart -= Time.deltaTime;
                delta = rasterSize / speed - animationStart;
                move = Move.NONE;
            }
            if (front)
                transform.Translate(0, 0, delta * speed);
            else
                transform.Translate(0, 0, -delta * speed);
            arm.transform.position = transform.position + transform.forward * 0.375f;
            if (movable)
                movable.transform.position += transform.position - oldPosition;
            foreach (var push in pushed)
                push.transform.position += transform.position - oldPosition;
            if (move == Move.NONE)
            {
                Movable.roundPosition(gameObject);
                Movable.roundPosition(arm.gameObject, 8);
                foreach (var push in pushed)
                    Movable.roundPosition(push.gameObject);
                if(pushed.Count > 0)
                    FindObjectOfType<GameController>().checkSolution();
            }
        }
        else if (move == Move.ROTATE_LEFT || move == Move.ROTATE_RIGHT)
        {
            float delta = Time.deltaTime;
            animationStart += Time.deltaTime;
            bool left = move == Move.ROTATE_LEFT;
            if (animationStart * rotationSpeed > 90)
            {
                animationStart -= Time.deltaTime;
                delta = 90 / rotationSpeed - animationStart;
                move = Move.NONE;
            }
            if (left)
            {
                transform.Rotate(0, -delta * rotationSpeed, 0);
                arm.transform.RotateAround(transform.position, Vector3.up, -delta * rotationSpeed);
                if (movable)
                    movable.transform.RotateAround(transform.position, Vector3.up, -delta * rotationSpeed);
            }
            else
            {
                transform.Rotate(0, delta * rotationSpeed, 0);
                arm.transform.RotateAround(transform.position, Vector3.up, delta * rotationSpeed);
                if (movable)
                    movable.transform.RotateAround(transform.position, Vector3.up, delta * rotationSpeed);
            }
            if (move == Move.NONE)
            {
                Movable.roundPosition(gameObject);
                Movable.roundPosition(arm.gameObject, 8);
                foreach (var push in pushed)
                    Movable.roundPosition(push.gameObject);
                if (pushed.Count > 0)
                    FindObjectOfType<GameController>().checkSolution();
            }
        }
        arm.transform.position = transform.position + transform.forward * 0.375f;
    }

    public bool ReverseMove()
    {
        if (!reversed)
        {
            if (move == Move.FRONT)
            {
                if (animationStart * speed > rasterSize * 0.9)
                    return false;
                move = Move.BACK;
                animationStart = (rasterSize - (animationStart * speed)) / speed;
            }
            else if (move == Move.BACK)
            {
                move = Move.FRONT;
                animationStart = (rasterSize - (animationStart * speed)) / speed;
            }
            else if (move == Move.ROTATE_LEFT)
            {
                move = Move.ROTATE_RIGHT;
                animationStart = (90 - (animationStart * rotationSpeed)) / rotationSpeed;
            }
            else if (move == Move.ROTATE_RIGHT)
            {
                move = Move.ROTATE_LEFT;
                animationStart = (90 - (animationStart * rotationSpeed)) / rotationSpeed;
            }
            reversed = true;
            return true;
        }
        return false;
    }

    public Move getCurrentMove()
    {
        return move;
    }

    public void addPushed(Movable movable)
    {
        if (movable && !pushed.Contains(movable) && !reversed)
        {
            movable.transform.position += transform.position - moveStartPoint;
            pushed.AddFirst(movable);
            return;
        }
        undoLastMove();
    }

    public bool isPushed(Movable movable)
    {
        return pushed.Contains(movable);
    }

    public void undoLastMove()
    {
        Debug.Log(moveStartPoint - transform.position);
        Movable movable = arm.getConnectedMovable();
        if (movable)
            movable.transform.position += moveStartPoint - transform.position;
        foreach (var push in pushed)
            push.transform.position += moveStartPoint - transform.position;
        transform.position += moveStartPoint - transform.position;
        arm.transform.position += transform.position + transform.forward * 0.375f;
    }

    private void OnTriggerEnter(Collider other)
    {
        Movable movable = Movable.getMovable(other.gameObject);
        if (move == Move.FRONT || move == Move.BACK)
        {
            if (movable)
                addPushed(movable);
            else
                ReverseMove();
        }
    }
}