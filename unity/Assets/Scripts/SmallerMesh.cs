﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallerMesh : MonoBehaviour
{
    public float FaceMovement = 0.05f;
    private Vector3[] newVertices;
    // Start is called before the first frame update
    void Start()
    {
        List<Vector3> uniqueVertices = new List<Vector3>();
        List<Vector3> uniqueNormals = new List<Vector3>();
        Mesh mesh = GetComponent<MeshCollider>().sharedMesh;
        List<Vector3> normals = new List<Vector3>();
        mesh.GetNormals(normals);
        if (normals.Count == 0)
        {
            mesh.RecalculateNormals();
            mesh.GetNormals(normals);
        }
        newVertices = new Vector3[mesh.vertexCount];
        for (int i = 0; i < mesh.vertexCount; i++)
        {
            bool found = false;
            for (int j = 0; j < uniqueVertices.Count; j++)
            {
                if (uniqueVertices[j].Equals(mesh.vertices[i]))
                {
                    found = true;
                    uniqueNormals[j] += normals[i];
                }
            }
            if (!found)
            {
                uniqueVertices.Add(mesh.vertices[i]);
                uniqueNormals.Add(normals[i]);
            }
        }
        for (int i = 0; i < uniqueNormals.Count; i++)
            uniqueNormals[i].Normalize();
        for (int i = 0; i < mesh.vertexCount; i++)
        {
            for (int j = 0; j < uniqueVertices.Count; j++)
            {
                if (uniqueVertices[j].Equals(mesh.vertices[i]))
                    newVertices[i] = mesh.vertices[i] - uniqueNormals[j] * FaceMovement;
            }
        }
        GetComponent<MeshCollider>().sharedMesh.vertices = newVertices;
        GetComponent<MeshCollider>().sharedMesh.RecalculateBounds();
    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<MeshCollider>().sharedMesh.vertices = newVertices;
        GetComponent<MeshCollider>().sharedMesh.RecalculateBounds();
    }
}
