﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movable : MonoBehaviour
{
    public bool highlighted = false;
    private Vector3 oldPosition;
    private bool moving = false;
    Color color = Color.white;

    // Start is called before the first frame update
    void Start()
    {
        oldPosition = transform.position;
        color = GetComponent<Renderer>().material.GetColor("_Color");
    }

    // Update is called once per frame
    void Update()
    {
        moving = transform.position != oldPosition;
        oldPosition = transform.position;
    }

    public void setHighlighted(bool hl)
    {
        var renderer = GetComponent<Renderer>();
        if (hl)
        {
            renderer.material.SetColor("_Color", Color.green);
            Vector3 center = renderer.bounds.center;
            Vector3 move = center - transform.position;
            move.Normalize();
            transform.position += new Vector3(move.x / 33f, 0.125f, move.z / 33f);
            transform.localScale = new Vector3((renderer.bounds.size.x - 0.06f) / renderer.bounds.size.x, 1f, (renderer.bounds.size.z - 0.06f) / renderer.bounds.size.z);
        }
        else if (highlighted)
        {
            renderer.material.SetColor("_Color", color);
            Vector3 center = renderer.bounds.center;
            Vector3 move = center - transform.position;
            move.Normalize();
            transform.position -= new Vector3(move.x / 33f, 0.125f, move.z / 33f);
            transform.localScale = new Vector3(1f, 1f, 1f);
            roundPosition(gameObject);
            FindObjectOfType<GameController>().checkSolution();
        }
        highlighted = hl;
    }

    private void OnTriggerEnter(Collider other)
    {
        Movable otherM = getMovable(other.gameObject);
        if (!other.GetComponent<PlayerArm>() && (hasMoved() || (otherM && otherM.hasMoved())))
        {
            if (other.gameObject.GetComponent<Player>())
                other.gameObject.GetComponent<Player>().addPushed(this);
            else if ((otherM) && (FindObjectOfType<Player>().isPushed(this) || FindObjectOfType<Player>().isPushed(otherM)))
            {
                FindObjectOfType<Player>().addPushed(this);
                FindObjectOfType<Player>().addPushed(otherM);
            }
            else
                FindObjectOfType<Player>().ReverseMove();
        }
    }

    private void OnTriggerStay(Collider other)
    {
        OnTriggerEnter(other);
    }

    public bool hasMoved()
    {
        return moving;
    }

    public static Movable getMovable(GameObject obj)
    {
        if (obj.GetComponent<Movable>())
            return obj.GetComponent<Movable>();
        return obj.GetComponentInParent<Movable>();
    }

    public static void roundPosition(GameObject obj, float factor = 4)
    {
        obj.transform.position = new Vector3(Mathf.Round(obj.transform.position.x * factor), obj.transform.position.y * factor, Mathf.Round(obj.transform.position.z * factor)) / factor;
    }
}
