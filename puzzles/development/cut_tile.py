import random

import numpy

from development.util import from_csv, to_csv

DIR = 'e'
FILE = DIR + '/original.png'
FROM_FILE = DIR + '/original.csv'
BLOCK_ID_FILE = DIR + '/block_ids.csv'
SCATTERED_BLOCK_ID_FILE = DIR + '/scattered_block_ids.csv'
TO_FILE = DIR + '/puzzle.csv'


def transform_coords(x, y, direction):
    if direction == 'U':
        return x - 1, y, 1
    elif direction == 'D':
        return x + 1, y, 0
    elif direction == 'R':
        return x, y + 1, 2
    elif direction == 'L':
        return x, y - 1, 3


def assign_blocks(puzzle_solution):
    block_ids = numpy.full(puzzle_solution.shape + (4,), fill_value=-1)  # last index is one id for U D L R in that order
    for (x, y), tile in numpy.ndenumerate(puzzle_solution):
        assert tile == puzzle_solution[x, y]
        assert tile not in ['L', 'R', 'U', 'D']  # otherwise the corner is not on the grid
        if tile in ['LR', 'UD', 'RL', 'DU']:
            raise NotImplementedError

        connections = []
        for direction in 'UDLR':
            if direction in tile:
                if block_ids[transform_coords(x, y, direction)] != -1:
                    connections += [direction] * 3
        if all(d in tile for d in 'UDLR'):
            connections += ['NUL/NDR']
            connections += ['NUR/NDL']
        if any(d in tile for d in 'UDLR'):
            connections += 'N'
        else:
            continue
        direction = random.choice(connections)
        next_block_id = block_ids.max() + 1
        if direction == 'N':
            for d in tile:
                block_ids[x, y, 'UDLR'.index(d)] = next_block_id
        elif '/' in direction:
            block_1, block_2 = direction.split('/')
            block_1_direction = random.choice([d for d in block_1 if d == 'N' or block_ids[transform_coords(x, y, d)] != -1])
            block_2_direction = random.choice([d for d in block_2 if d == 'N' or block_ids[transform_coords(x, y, d)] != -1])
            for d in block_1.replace('N', ''):
                if block_1_direction == 'N':
                    block_ids[x, y, 'UDLR'.index(d)] = next_block_id
                else:
                    block_ids[x, y, 'UDLR'.index(d)] = block_ids[transform_coords(x, y, block_1_direction)]
            next_block_id = block_ids.max() + 1
            for d in block_2.replace('N', ''):
                if block_2_direction == 'N':
                    block_ids[x, y, 'UDLR'.index(d)] = next_block_id
                else:
                    block_ids[x, y, 'UDLR'.index(d)] = block_ids[transform_coords(x, y, block_2_direction)]
        elif direction in 'UDLR':
            for d in tile:
                block_ids[x, y, 'UDLR'.index(d)] = block_ids[transform_coords(x, y, direction)]
        else:
            raise LogicError()

        # connections = []
        # for direction in 'UDLR':
        #     if direction in tile:
        #         if block_ids[transform_coords(x, y, direction)] != -1:
        #             connections.append(direction)
        #     if all(d in tile for d in 'UDLR'):
        #         connections += ['UL/DR'] * 4
        #         connections += ['UR/DL'] * 4
        # if tile != '':
        #     connections.append('N')
        # else:
        #     continue
        # direction = random.choice(connections)
        # if direction == 'N':
        #     block_ids[x, y] = numpy.max(block_ids) + 1
        # elif '/' in direction:
        #     block_1, block_2 = direction.split('/')
        #     block_1_direction = random.choice(block_1)
        #     block_2_direction = random.choice(block_2)
        #     for d in block_1:
        #         block_ids[x, y, 'UDLR'.index(d)] = block_ids[transform_coords(x, y, block_1_direction)]
        #     for d in block_2:
        #         block_ids[x, y, 'UDLR'.index(d)] = block_ids[transform_coords(x, y, block_2_direction)]
        # elif direction in 'UDLR':
        #     block_ids[x, y] = block_ids[transform_coords(x, y, direction)]
        # else:
        #     raise LogicError()
    return block_ids


class MaxIterations(Exception):
    pass


class LogicError(Exception):
    pass


def block_id_to_str(block_ids, x, y):
    result = ''
    for idx in range(4):
        result += 'UDLR'[idx]
        if idx == 3 or block_ids[x, y, idx] != block_ids[x, y, idx + 1]:
            result += str(block_ids[x, y, idx])
    return result.replace('UDLR', '')


def main():
    puzzle_solution = from_csv(FROM_FILE)

    # add a player at the top left
    assert puzzle_solution[0, 0] == ''

    block_ids = assign_blocks(puzzle_solution)
    string_ids = [[block_id_to_str(block_ids, x, y)
                   for y in range(block_ids.shape[1])]
                  for x in range(block_ids.shape[0])]
    to_csv(numpy.array(string_ids, dtype=object), filename=BLOCK_ID_FILE)
    max_tries_outer = 1000
    max_tries_inner = 1000
    scattered_block_ids = numpy.full(block_ids.shape, fill_value=-1)
    for _ in range(max_tries_outer):
        for block_id in range(block_ids.max() + 1):
            for _ in range(max_tries_inner):
                x = random.randrange(0, puzzle_solution.shape[0])
                y = random.randrange(0, puzzle_solution.shape[1])
                block_indices = numpy.argwhere(block_ids == block_id)
                current_x, current_y, _ = block_indices.min(axis=0)
                block_indices_relative = block_indices - [current_x, current_y, 0]
                block_height, block_width, _ = block_indices_relative.max(axis=0) + 1
                block_fits = (not x == y == 0
                              and x + block_height < puzzle_solution.shape[0]
                              and y + block_width < puzzle_solution.shape[1]
                              and all(scattered_block_ids[x2, y2, d2] == -1
                                      for x2, y2, d2 in block_indices_relative + [x, y, 0]))
                if block_fits:
                    for x2, y2, d2 in block_indices_relative + [x, y, 0]:
                        scattered_block_ids[x2, y2, d2] = block_id
                    break
            else:
                break
        break
    else:
        raise MaxIterations

    string_ids = [[block_id_to_str(scattered_block_ids, x, y)
                   for y in range(scattered_block_ids.shape[1])]
                  for x in range(scattered_block_ids.shape[0])]
    to_csv(numpy.array(string_ids, dtype=object), filename=SCATTERED_BLOCK_ID_FILE)

    puzzle = numpy.full(puzzle_solution.shape, '', puzzle_solution.dtype)
    for (x, y, d), block_id in numpy.ndenumerate(scattered_block_ids):
        if block_id != -1:
            puzzle[x, y] += 'UDLR'[d]
    assert puzzle[0, 0] == ''
    puzzle[0, 0] = 'P'
    to_csv(puzzle, filename=TO_FILE)


if __name__ == '__main__':
    main()
