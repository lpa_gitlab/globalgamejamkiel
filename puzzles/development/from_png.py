import imageio
import numpy

from development.util import to_csv
DIR = 'e'
FILE = DIR + '/original.png'
TO_FILE = DIR + '/original.csv'

png = imageio.imread(FILE)
png = numpy.mean(png, axis=-1)

as_strings = numpy.full(shape=png.shape,
                        fill_value='',
                        dtype=object)
for x in range(png.shape[0]):
    for y in range(png.shape[1]):
        if png[x, y] == 0:
            as_strings[x,y] = 'UDLR'

to_csv(as_strings, filename=TO_FILE)
