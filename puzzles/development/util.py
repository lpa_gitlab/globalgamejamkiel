import pandas


def to_csv(x, filename):
    pandas.DataFrame(x).to_csv(filename, sep=';', header=False, index=False)


def from_csv(filename):
    data = pandas.read_csv(filename, sep=';', header=None, keep_default_na=False, dtype=object)
    return data.to_numpy(dtype=object)
