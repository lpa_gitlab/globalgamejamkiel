﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using System.Linq;
using System.Text;
using System.IO;


public class GameController : MonoBehaviour
{
    [Header("Zur Szene")]
    public string levelName;

    [Header("Main Menu")]
    public GameObject MenuCanvas;
    private bool Paused = false;
    private bool levelWon = false;
    public float yOffset = 0;

    // H
    public GameObject gesamtH;
    public GameObject H1;
    public GameObject H2;
    public GameObject H3;
    public GameObject H4;
    public GameObject H5;
    public GameObject H6;

    Vector2Int rasterH1;
    Vector2Int rasterH2;
    Vector2Int rasterH3;
    Vector2Int rasterH4;
    Vector2Int rasterH5;
    Vector2Int rasterH6;

    // SPIELFELD
    private Vector2 topLeft = new Vector2(0.0f, 0.0f);
    private float rasterSize = 0.25f;

    Vector2 topLeftMap;
    Vector2 bottomRightMap;
    int h_cells;
    int w_cells;
    RaycastHit hit;
    int[,] map;
    bool[,] cur_sit;
    int[,] cur_map;
    int[,] cur_diff;
    Vector3 originTmp; // = new Vector3(-2.8f, 2.25f, 0.2f);
    Vector3 topLeftWC;
    Vector3 shift;
    float radius;
    Collider[] hitColliders;
    bool patternFound;

    // PATTERN
    int[,] patternH_int;
    int[,] patternI_int;
    int patternH_size_x;
    int patternH_size_y;
    int patternI_size_x;
    int patternI_size_y;
    int pattern_size_x;
    int pattern_size_y;
    int[,] pattern_int;


    // bool[,] patternH = new bool[,]
    // {
    //     {true, true, true, false, true, true, true},
    //     {true, true, true, false, true, true, true},
    //     {true, true, true, false, true, true, true},
    //     {true, true, true, true, true, true, true},
    //     {true, true, true, true, true, true, true},
    //     {true, true, true, true, true, true, true},
    //     {true, true, true, true, true, true, true},
    //     {true, true, true, false, true, true, true},
    //     {true, true, true, false, true, true, true}
    // };

    // Start is called before the first frame update
    void Start()
    {
        topLeftMap = new Vector2(-7.0f, 5.0f);
        bottomRightMap = new Vector2(7.0f, -5.0f);
        h_cells = (int)Math.Round((bottomRightMap.x - topLeftMap.x) / rasterSize);
        w_cells = (int)Math.Round((topLeftMap.y - bottomRightMap.y) / rasterSize);
        //Debug.Log("H: " + h_cells + ", W: " + w_cells);
        map = new int[h_cells, w_cells];
        cur_sit = new bool[h_cells, w_cells];
        cur_map = new int[h_cells, w_cells];
        cur_diff = new int[h_cells, w_cells];
        topLeftWC = new Vector3(topLeftMap.x, 0.0f, topLeftMap.y);
        radius = .5f * rasterSize;

        patternH_int = new int[,] {
            {1, 1, 1, 0, 1, 1, 1},
            {1, 1, 1, 0, 1, 1, 2},
            {1, 1, 2, 0, 1, 2, 1},
            {1, 2, 1, 1, 3, 1, 1},
            {2, 1, 1, 2, 1, 1, 2},
            {1, 1, 3, 1, 1, 2, 1},
            {1, 2, 1, 0, 2, 1, 1},
            {2, 1, 1, 0, 1, 1, 1},
            {1, 1, 1, 0, 1, 1, 1}
        };
        pattern_size_x = 7;
        pattern_size_y = 9;
        patternI_int = new int[,] {
            {1, 1, 1, 1, 1, 1, 1, 1, 2},
            {1, 1, 1, 1, 1, 1, 1, 2, 2},
            {1, 1, 1, 1, 1, 1, 2, 2, 1},
            {0, 1, 1, 1, 1, 1, 1, 1, 0},
            {0, 0, 2, 2, 2, 2, 2, 0, 0},
            {0, 0, 1, 1, 1, 1, 1, 0, 0},
            {0, 0, 1, 1, 1, 1, 1, 0, 0},
            {0, 0, 1, 1, 1, 1, 1, 0, 0},
            {0, 0, 2, 2, 2, 2, 2, 0, 0},
            {0, 0, 1, 2, 2, 2, 1, 0, 0},
            {0, 0, 1, 1, 2, 1, 1, 0, 0},
            {0, 0, 1, 1, 1, 1, 1, 0, 0},
            {0, 0, 2, 2, 2, 2, 2, 0, 0},
            {0, 1, 1, 2, 2, 2, 1, 1, 0},
            {1, 1, 1, 1, 2, 1, 1, 1, 1},
            {1, 1, 1, 1, 1, 1, 1, 1, 1},
            {1, 1, 1, 1, 1, 1, 1, 1, 1}
        };
        patternI_size_x = 7;
        patternI_size_y = 9;
    }

    // Update is called once per frame
    void Update()
    {
        // ---------------------------------------------------------------- Main Menu
        if (Input.GetKey("escape"))
        {
            if (Paused == true)
            {
                //Time.timeScale = 1.0f;
                MenuCanvas.SetActive(false);
                Paused = false;
            }
            else
            {
                //Time.timeScale = 0.0f;
                MenuCanvas.SetActive(true);
                Paused = true;
            }
        }
        if (Input.GetKeyDown(KeyCode.U))
        {
            saveSolution();
        }
        if (Input.GetKeyDown(KeyCode.H))
        {
            bool correct = checkSolution("H");
            if (correct)
            {
                Debug.Log("WWWWWOOON!!!!");
            }
        }
        if (Input.GetKeyDown(KeyCode.I))
        {
            bool correct = checkSolution("I");
            if (correct)
            {
                Debug.Log("WWWWWOOON!!!!");
            }
        }
        if (Input.GetKeyDown(KeyCode.J))
        {
            saveTemporarySituation("H");
        }
        if (Input.GetKeyDown(KeyCode.O))
        {
            saveTemporarySituation("I");
        }
    }

    // -------------------------------------------------------------------- Save .txt with solution
    public void saveSolution()
    {
        //Paused = true;
        int ij = 0;
        for (int i = 0; i < h_cells; i++)
        {
            for (int j = 0; j < w_cells; j++)
            {
                shift = new Vector3(i * rasterSize, yOffset, -j * rasterSize);
                originTmp = topLeftWC + shift;
                ij = 0;
                hitColliders = Physics.OverlapSphere(originTmp, radius);
                int counter = 0;
                while (ij < hitColliders.Length)
                {
                    if (Movable.getMovable(hitColliders[ij].gameObject))
                    {
                        counter++;
                    }
                    ij++;
                }
                map[i, j] = counter;
            }
        }
        StreamWriter writer = new StreamWriter("/Users/Palma/Documents/Projects/GameJam2020/solution_" + Time.time.ToString("f6") + ".txt", false);
        string line;
        for (int j=0; j<w_cells; j++)
        {
            line = "";
            for (int i=0; i<h_cells; i++)
            {
                line = line + map[i,j] + ", ";
            }
            writer.WriteLine(line);
        }
        writer.Close();
    }

    // -------------------------------------------------------------------- Check if pieces match
    public bool checkSolution(string letter)
    {
        //Debug.Log("Checking!");
        //Paused = true;
        if (letter == "H")
        {
            pattern_size_x = patternH_size_x;
            pattern_size_y = patternH_size_y;
            pattern_int = patternH_int;
        }   
        else
        {
            pattern_size_x = patternI_size_x;
            pattern_size_y = patternI_size_y;
            pattern_int = patternI_int;
        }

        int i_half_pat_size = (int)(Mathf.Floor(pattern_size_x / 2));
        int j_half_pat_size = (int)(Mathf.Floor(pattern_size_y / 2));
        int ij = 0;
        int diff = 0;
        for (int i = 0; i < h_cells-1; i++)
        {
            for (int j = 0; j < w_cells-1; j++)
            {
                shift = new Vector3(i * rasterSize, yOffset, -j * rasterSize);
                originTmp = topLeftWC + shift;
                ij = 0;
                int counter = 0;
                hitColliders = Physics.OverlapSphere(originTmp, radius);
                while (ij < hitColliders.Length)
                {
                    //Debug.Log("At raster cell (" + i + ", " + j + "), with 3D position " + originTmp + ", overlap with " + hitColliders[ij] + " with position " + hitColliders[ij].transform.position); //.SendMessage("AddDamage");
                    if (Movable.getMovable(hitColliders[ij].gameObject))
                    {
                        counter++;
                    }
                    ij++;
                }
                cur_map[i, j] = counter;
                if (ij > 0)
                    cur_sit[i, j] = true;
                else
                    cur_sit[i, j] = false;
            }
        }
        patternFound = false;
        // jetzt checken
        for (int i = i_half_pat_size + 1; i < h_cells - pattern_size_x - 1; i++)
        {
            for (int j = j_half_pat_size + 1; j < w_cells - pattern_size_y - 1; j++)
            {
                if (cur_map[i, j] > 0)
                {
                    //Debug.Log("checking at " + i + ", " + j);
                    diff = checkPatternI(pattern_int, cur_map, i, j, pattern_size_x, pattern_size_y);
                    if (diff < 1)
                    {
                        patternFound = true;
                    }
                    cur_diff[i, j] = diff;
                }
            }
        }
        Debug.Log("Found? " + patternFound);
        return patternFound;
    }


    public void saveTemporarySituation(string letter)
    {
        if (letter == "H")
        {
            pattern_size_x = patternH_size_x;
            pattern_size_y = patternH_size_y;
            pattern_int = patternH_int;
        }   
        else
        {
            pattern_size_x = patternI_size_x;
            pattern_size_y = patternI_size_y;
            pattern_int = patternI_int;
        }
        int i_half_pat_size = (int)(Mathf.Floor(pattern_size_x / 2));
        int j_half_pat_size = (int)(Mathf.Floor(pattern_size_y / 2));
        int ij = 0;
        int diff = 0;
        for (int i = 0; i < h_cells-1; i++)
        {
            for (int j = 0; j < w_cells-1; j++)
            {
                shift = new Vector3(i * rasterSize, yOffset, -j * rasterSize);
                originTmp = topLeftWC + shift;
                ij = 0;
                int counter = 0;
                hitColliders = Physics.OverlapSphere(originTmp, radius);
                while (ij < hitColliders.Length)
                {
                    //Debug.Log("At raster cell (" + i + ", " + j + "), with 3D position " + originTmp + ", overlap with " + hitColliders[ij] + " with position " + hitColliders[ij].transform.position); //.SendMessage("AddDamage");
                    if (Movable.getMovable(hitColliders[ij].gameObject))
                    {
                        counter++;
                    }
                    ij++;
                }
                cur_map[i, j] = counter;
                if (ij > 0)
                    cur_sit[i, j] = true;
                else
                    cur_sit[i, j] = false;
            }
        }
        StreamWriter writer = new StreamWriter("/Users/Palma/Documents/Projects/GameJam2020/cur_map_" + Time.time.ToString("f6") + ".txt", false);
        string line;
        for (int j=0; j<w_cells; j++)
        {
            line = "";
            for (int i=0; i<h_cells; i++)
            {
                line = line + cur_map[i,j] + ", ";
            }
            writer.WriteLine(line);
        }
        writer.Close();
        patternFound = false;
        // jetzt checken
        for (int i = i_half_pat_size + 1; i < h_cells - pattern_size_x - 1; i++)
        {
            for (int j = j_half_pat_size + 1; j < w_cells - pattern_size_y - 1; j++)
            {
                if (cur_map[i, j] > 0)
                {
                    //Debug.Log("checking at " + i + ", " + j);
                    diff = checkPatternI(pattern_int, cur_map, i, j, pattern_size_x, pattern_size_y);
                    if (diff < 1)
                    {
                        patternFound = true;
                    }
                    cur_diff[i, j] = diff;
                }
            }
        }
        writer = new StreamWriter("/Users/Palma/Documents/Projects/GameJam2020/cur_diff_" + Time.time.ToString("f6") + ".txt", false);
        for (int j=0; j<w_cells; j++)
        {
            line = "";
            for (int i=0; i<h_cells; i++)
            {
                line = line + cur_diff[i,j] + ", ";
            }
            writer.WriteLine(line);
        }
        writer.Close();

    }

    bool checkPattern(bool[,] pattern, bool[,] map, int x, int y, int l_x, int l_y)
    {
        int diff = 0;
        for (int i = 0; i < l_x - 1; i++)
        {
            for (int j = 0; j < l_y - 1; j++)
            {
                if (pattern[i, j] != map[x + i, y + j])
                {
                    diff += 1;
                }
            }
        }

        if (diff < 1)
            return true;
        //Debug.Log("Diff: " + diff);
        return false;
    }

    int checkPatternI(int[,] pattern, int[,] map, int x, int y, int l_x, int l_y)
    {
        int diff = 0;
        //Debug.Log("At " + x + ", " + y + ", l_x " + l_x + ", l_y " + l_y);
        //Debug.Log(pattern);
        for (int i = 0; i < (l_x - 1); i++)
        {
            for (int j = 0; j < l_y - 1; j++)
            {
                //Debug.Log("At " + i + ", " + j + " l " + l_x);
                //Debug.Log("At " + i + ", " + j + "pattern: " + patternH_int[i,j]);
                if (pattern[j, i] > 0 && map[x + i, y + j] == 0 || pattern[j, i] == 0 && map[x + i, y + j] > 0)
                {
                    diff += 1; //(Math.Abs(pattern[j,i] - map[x+i,y+j]));
                }
            }
        }
        //Debug.Log("At " + x + ", " + y + ", diff: " + diff);
        return diff;
    }

    // -------------------------------------------------------------------- Get Raster Cell from 3D Position
    public Vector2Int getRasterCell(Vector3 pos)
    {
        // y is the plane of the game
        Vector2 absolutePosition = new Vector2(pos.x, pos.z);
        Vector2 relativePosition = absolutePosition - topLeft;
        int i = (int)(Mathf.RoundToInt(relativePosition.x / rasterSize));
        int j = (int)(Mathf.RoundToInt(relativePosition.y / rasterSize));
        Vector2Int rasterCell = new Vector2Int(i, j);
        return rasterCell;
    }

    // -------------------------------------------------------------------- Get 3D Position from Raster Cell
    public Vector3 getPosition(Vector2Int rasterCell)
    {
        float x_position = rasterCell.x * rasterSize + topLeft.x;
        float z_position = rasterCell.y * rasterSize + topLeft.y;
        Vector3 xyz_position = new Vector3(x_position, 0.0f, z_position);
        return xyz_position;
    }

    // ---------------------------------------------------------------- Resume Menu function
    public void Resume()
    {
        Time.timeScale = 1.0f;
        MenuCanvas.SetActive(false);
    }

    // ---------------------------------------------------------------- To Main Menu function
    public void toMainMenu()
    {
        SceneManager.LoadScene(levelName);
    }

    public bool isPaused()
    {
        return Paused;
    }
}
